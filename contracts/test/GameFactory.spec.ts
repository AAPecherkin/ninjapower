import { ethers, upgrades } from "hardhat";
import { expect } from "chai";
import { Contract, Signer } from "ethers";

import { GameFactory, NinjaPower, MockedAurora } from "../typechain-types";
import { getCurrentGame } from "../src";
import { time } from '@nomicfoundation/hardhat-network-helpers';

describe("GameFactory", function () {
//   let GameFactory;
  let assetToken: MockedAurora;
  let gameFactory: GameFactory;
//   let NinjaPowerContract;
  let NinjaPowerContract: NinjaPower;
  let NinjaPowerContractV2: NinjaPower;
  let owner: Signer;
  let player1: Signer;
  let player2: Signer;
  let addrs;

  beforeEach(async function () {
    [owner, player1, player2, ...addrs] = await ethers.getSigners();

    // Deploy the ERC20 token contract
    const Aurora = await ethers.getContractFactory("MockedAurora");
    assetToken = await Aurora.deploy("Test Token", "TEST");

    const NinjaPowerContract = await ethers.getContractFactory("NinjaPower");
    NinjaPowerContract = await NinjaPowerContract.deploy();

    const GameFactory = await ethers.getContractFactory("GameFactory");
    const rewardAmount = ethers.utils.parseEther("100");
    // gameFactory = await GameFactory.deploy(NinjaPowerContract.address, assetToken.address, rewardAmount);
    gameFactory = await upgrades.deployProxy(GameFactory, [NinjaPowerContract.address, assetToken.address, rewardAmount]) as GameFactory;

    // Transfer some tokens to the players
    await assetToken.mint(gameFactory.address, ethers.utils.parseEther("1000"));


    // await NinjaPowerContract.connect(owner).setGameFactoryAddress(gameFactory.address);
  });

  describe("Deployment", function () {
    it("Should set the correct master contract address", async function () {
      expect(await gameFactory.masterContract()).to.equal(NinjaPowerContract.address);
    });
  });

  describe("Inactive contract", function () {
    it("contract should be inactive", async function () {
      const poolGame = await getCurrentGame(gameFactory);

      const isActive = await poolGame.isActive();
      expect(isActive).to.be.false;

      await assetToken.mint(await player1.getAddress(), ethers.utils.parseEther("100"));
      await assetToken.connect(player1).approve(poolGame.address, ethers.utils.parseEther("100"));
      await expect(gameFactory.connect(player1).sendToPool1(ethers.utils.parseEther("100"))).to.not.be.reverted;
    });

    it("contract should be active", async function () {
      const poolGame = await getCurrentGame(gameFactory);

      await assetToken.mint(await player1.getAddress(), ethers.utils.parseEther("100"));
      await assetToken.connect(player1).approve(poolGame.address, ethers.utils.parseEther("100"));
      await expect(gameFactory.connect(player1).sendToPool1(ethers.utils.parseEther("100"))).to.not.be.reverted;

      const isActive = await poolGame.isActive();
      expect(isActive).to.be.true;
    });
  });

  describe("Call proxies", function () {
    it("should call sendPool1", async function () {
      // await gameFactory.createContract(assetToken.address, ethers.utils.parseEther("100"));
      
      const poolGame = await getCurrentGame(gameFactory);
      // Transfer some tokens to the players
      await assetToken.mint(await player1.getAddress(), ethers.utils.parseEther("100"));
      await assetToken.mint(await player2.getAddress(), ethers.utils.parseEther("100"));

      // Approve the pool game contract to spend tokens on behalf of the players
      await assetToken.connect(player1).approve(poolGame.address, ethers.utils.parseEther("100"));
      await assetToken.connect(player2).approve(poolGame.address, ethers.utils.parseEther("100"));
      
      await expect(gameFactory.connect(player1).sendToPool1(ethers.utils.parseEther("100"))).to.not.be.reverted;
      await expect(gameFactory.connect(player2).sendToPool2(ethers.utils.parseEther("100"))).to.not.be.reverted;
    });

    it("should call withraws", async function () {
      // await gameFactory.createContract(assetToken.address, ethers.utils.parseEther("100"));
      
      const poolGame = await getCurrentGame(gameFactory);
      // Transfer some tokens to the players
      await assetToken.mint(await player1.getAddress(), ethers.utils.parseEther("100"));
      await assetToken.mint(await player2.getAddress(), ethers.utils.parseEther("200"));

      // Approve the pool game contract to spend tokens on behalf of the players
      await assetToken.connect(player1).approve(poolGame.address, ethers.utils.parseEther("100"));
      await assetToken.connect(player2).approve(poolGame.address, ethers.utils.parseEther("200"));

      await poolGame.connect(player1).sendToPool1(ethers.utils.parseEther("100"));
      await poolGame.connect(player2).sendToPool2(ethers.utils.parseEther("200"));

      await ethers.provider.send("evm_increaseTime", [24 * 60 * 60]); // Increase time to 24 hours later

      await expect(gameFactory.connect(player1).withdraw(await player1.getAddress())).to.not.be.reverted;
      await expect(gameFactory.connect(player2).withdraw(await player2.getAddress())).to.not.be.reverted;
    });

    it("should call withraw for two games", async function () {
      const timestamp = await time.latest();
      await assetToken.mint(await player1.getAddress(), ethers.utils.parseEther("200"));
      const balanceBefore = await assetToken.balanceOf(await player1.getAddress());

      // await gameFactory.createContract(assetToken.address, ethers.utils.parseEther("100"));
      
      const poolGame = await getCurrentGame(gameFactory);
      // Transfer some tokens to the players
      // await assetToken.mint(await player1.getAddress(), ethers.utils.parseEther("100"));

      // Approve the pool game contract to spend tokens on behalf of the players
      await assetToken.connect(player1).approve(poolGame.address, ethers.utils.parseEther("100"));
      await gameFactory.connect(player1).sendToPool1(ethers.utils.parseEther("100"));

      await time.increaseTo(timestamp + 24 * 60 * 60); // Increase time to 24 hours later

      // new game
      // await gameFactory.createContract(assetToken.address, ethers.utils.parseEther("100"));
      const poolGame2 = await getCurrentGame(gameFactory);
 
      // await assetToken.mint(await player1.getAddress(), ethers.utils.parseEther("100"));
      await assetToken.connect(player1).approve(poolGame2.address, ethers.utils.parseEther("100"));
      await gameFactory.connect(player1).sendToPool1(ethers.utils.parseEther("100"));

      await time.increaseTo(timestamp + 48 * 60 * 60); // Increase time to 24 hours later

      await expect(gameFactory.connect(player1).withdraw(await player1.getAddress())).to.not.be.reverted;
      const balanceAfter = await assetToken.balanceOf(await player1.getAddress());
      expect(balanceAfter.sub(balanceBefore)).to.equal(ethers.utils.parseEther("200"));

      // await gameFactory.createContract(assetToken.address, ethers.utils.parseEther("100"));
      const poolGame3 = await getCurrentGame(gameFactory);

      await assetToken.mint(await player1.getAddress(), ethers.utils.parseEther("100"));
      await assetToken.connect(player1).approve(poolGame3.address, ethers.utils.parseEther("100"));
      await gameFactory.connect(player1).sendToPool1(ethers.utils.parseEther("100"));

      await time.increaseTo(timestamp + 72 * 60 * 60); // Increase time to 24 hours later

      await expect(gameFactory.connect(player1).withdraw(await player1.getAddress())).to.changeTokenBalance(
        assetToken, 
        player1, 
        ethers.utils.parseEther("200")
      );
    });

    it("shouldn't withraw anything for active games", async function () {
      const timestamp = await time.latest();
      await assetToken.mint(await player1.getAddress(), ethers.utils.parseEther("300"));
      // const balanceBefore = await assetToken.balanceOf(await player1.getAddress());

      // first game
      // await gameFactory.createContract(assetToken.address, ethers.utils.parseEther("100"));
      
      const poolGame = await getCurrentGame(gameFactory);
      // Transfer some tokens to the players
      // await assetToken.mint(await player1.getAddress(), ethers.utils.parseEther("100"));

      // Approve the pool game contract to spend tokens on behalf of the players
      await assetToken.connect(player1).approve(poolGame.address, ethers.utils.parseEther("100"));
      await gameFactory.connect(player1).sendToPool1(ethers.utils.parseEther("100"));

      await expect(gameFactory.connect(player1).withdraw(await player1.getAddress())).to.changeTokenBalance(
        assetToken,
        player1,
        ethers.utils.parseEther("0")
      );

      await time.increaseTo(timestamp + 24 * 60 * 60); // Increase time to 24 hours later

      // second game
      // await gameFactory.createContract(assetToken.address, ethers.utils.parseEther("100"));
      const secondPoolGame = await getCurrentGame(gameFactory);
      await assetToken.connect(player1).approve(secondPoolGame.address, ethers.utils.parseEther("100"));
      await gameFactory.connect(player1).sendToPool1(ethers.utils.parseEther("100"));

      await expect(gameFactory.connect(player1).withdraw(await player1.getAddress())).to.changeTokenBalance(
        assetToken,
        player1,
        ethers.utils.parseEther("200")
      );

      await time.increaseTo(timestamp + 48 * 60 * 60); // Increase time to 24 hours later

      // seconds game ended
      await expect(gameFactory.connect(player1).withdraw(await player1.getAddress())).to.changeTokenBalance(
        assetToken,
        player1,
        ethers.utils.parseEther("200")
      );
    });

  });
});