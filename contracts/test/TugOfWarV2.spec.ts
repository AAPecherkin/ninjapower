import { ethers } from "hardhat";
import { BigNumber, Contract, Signer } from "ethers";
import { expect } from "chai";
import { time } from '@nomicfoundation/hardhat-network-helpers';
import { MockedAurora, NinjaPowerV2 } from "../typechain-types";

const SECONDS_IN_HOUR = 60 * 60;
const SECONDS_IN_MIN = 60;

describe("NinjaPowerV2", function () {
  let owner: Signer;
  let player1: Signer;
  let player2: Signer;
  let player3: Signer;
  let assetToken: MockedAurora;
  let poolGame: NinjaPowerV2;

  beforeEach(async function () {
    [owner, player1, player2, player3] = await ethers.getSigners();

    // Deploy the ERC20 token contract
    const Aurora = await ethers.getContractFactory("MockedAurora");
    assetToken = await Aurora.deploy("Test Token", "TEST");

    // Deploy the pool game contract
    const PoolGame = await ethers.getContractFactory("NinjaPowerV2");
    poolGame = await PoolGame.deploy();

    // Transfer some tokens to the players
    await assetToken.mint(await owner.getAddress(), ethers.utils.parseEther("100"));
    await assetToken.mint(await player1.getAddress(), ethers.utils.parseEther("1000"));
    await assetToken.mint(await player2.getAddress(), ethers.utils.parseEther("1000"));
    await assetToken.mint(await player3.getAddress(), ethers.utils.parseEther("1000"));

    // Approve the pool game contract to spend tokens on behalf of the players
    await assetToken.connect(owner).approve(poolGame.address, ethers.utils.parseEther("100"));
    await assetToken.connect(player1).approve(poolGame.address, ethers.utils.parseEther("1000"));
    await assetToken.connect(player2).approve(poolGame.address, ethers.utils.parseEther("1000"));
    await assetToken.connect(player3).approve(poolGame.address, ethers.utils.parseEther("1000"));
  });

  it("should not allow sending tokens to pools before the game starts", async function () {
    await expect(poolGame.connect(player1).sendToPool1(ethers.utils.parseEther("100"))).to.be.revertedWith("Game has not started yet.");
    await expect(poolGame.connect(player2).sendToPool2(ethers.utils.parseEther("100"))).to.be.revertedWith("Game has not started yet.");
  });

  it("should allow sending tokens to pools after the game starts", async function () {
    const gameEndTime = await time.latest() + 23 * SECONDS_IN_HOUR + 55 * SECONDS_IN_MIN;
    await poolGame.connect(owner).startGame(assetToken.address, 0, gameEndTime);
    await expect(poolGame.connect(player1).sendToPool1(ethers.utils.parseEther("100"))).to.not.be.reverted;
    await expect(poolGame.connect(player2).sendToPool2(ethers.utils.parseEther("100"))).to.not.be.reverted;
  });

  it("should not allow sending tokens to pools after the game ends", async function () {
    const gameEndTime = await time.latest() + 23 * SECONDS_IN_HOUR + 55 * SECONDS_IN_MIN;
    await poolGame.connect(owner).startGame(assetToken.address, 0, gameEndTime);
    await ethers.provider.send("evm_increaseTime", [24 * 60 * 60]); // Increase time to 24 hours later
    await expect(poolGame.connect(player1).sendToPool1(ethers.utils.parseEther("100"))).to.be.revertedWith("Game has ended.");
    await expect(poolGame.connect(player2).sendToPool2(ethers.utils.parseEther("100"))).to.be.revertedWith("Game has ended.");
  });

  it("should allow withdrawing tokens after the game ends", async function () {
    const player1Address = await player1.getAddress();
    const player2Address = await player2.getAddress();

    const gameEndTime = await time.latest() + 23 * SECONDS_IN_HOUR + 55 * SECONDS_IN_MIN;
    await poolGame.connect(owner).startGame(assetToken.address, 0, gameEndTime);
    await poolGame.connect(player1).sendToPool1(ethers.utils.parseEther("100"));
    await poolGame.connect(player2).sendToPool2(ethers.utils.parseEther("200"));
  
    await ethers.provider.send("evm_increaseTime", [24 * 60 * 60]); // Increase time to 24 hours later
  
    expect(await poolGame.previewWithdrawal(player1Address)).to.eq(0);

    await expect(poolGame.connect(player2).withdraw(player2Address))
        .to
        .changeTokenBalance(assetToken, player2, ethers.utils.parseEther("300").mul(99).div(100));
  });

  it("should multiply points", async function () {
    const player1Address = await player1.getAddress();

    const value = ethers.utils.parseEther("1");
    const gameEndTime = await time.latest() + 23 * SECONDS_IN_HOUR + 55 * SECONDS_IN_MIN;
    await poolGame.connect(owner).startGame(assetToken.address, 0, gameEndTime);
    
    await poolGame.connect(player1).sendToPool1(value);
    const points100 = await poolGame.pool1PointsBalances(player1Address);
    expect(points100).to.equal(value.mul(100));

    await ethers.provider.send("evm_increaseTime", [2 * 60 * 60]);

    await poolGame.connect(player1).sendToPool1(value);
    const points90 = (await poolGame.pool1PointsBalances(player1Address)).sub(points100);
    expect(points90).to.equal(value.mul(90));
  
    await ethers.provider.send("evm_increaseTime", [(21 * 60 + 50) * 60]);

    await poolGame.connect(player1).sendToPool1(value);
    const points2 = (await poolGame.pool1PointsBalances(player1Address)).sub(points100.add(points90));
    expect(points2).to.equal(value.mul(2));
  });

  it("should distribute rewards proportionally to winner points", async function () {
    const player1Address = await player1.getAddress();
    const player2Address = await player2.getAddress();
    const player3Address = await player3.getAddress();

    // const player1BalanceBefore = await assetToken.balanceOf(player1Address);
    // const player2BalanceBefore = await assetToken.balanceOf(player2Address);

    const gameEndTime = await time.latest() + 23 * SECONDS_IN_HOUR + 55 * SECONDS_IN_MIN;
    await poolGame.connect(owner).startGame(assetToken.address, 0, gameEndTime);
    await poolGame.connect(player1).sendToPool1(ethers.utils.parseEther("200"));
    await poolGame.connect(player2).sendToPool1(ethers.utils.parseEther("400"));
    await poolGame.connect(player3).sendToPool2(ethers.utils.parseEther("300"));
  
    await ethers.provider.send("evm_increaseTime", [24 * 60 * 60]); // Increase time to 24 hours later

    await expect(poolGame.withdraw(player1Address))
        .to
        .changeTokenBalance(assetToken, player1, ethers.utils.parseEther("297")); // 300 x 99%

    await expect(poolGame.withdraw(player2Address))
        .to
        .changeTokenBalance(assetToken, player2, ethers.utils.parseEther("594")); // 600 x 99%

    expect(await poolGame.previewWithdrawal(player3Address))
        .to
        .eq(0);
  });

  it("should return deposits in case of draw", async function () {
    const player1Address = await player1.getAddress();
    const player2Address = await player2.getAddress();
    const player3Address = await player3.getAddress();

    const gameEndTime = await time.latest() + 23 * SECONDS_IN_HOUR + 55 * SECONDS_IN_MIN;
    await poolGame.connect(owner).startGame(assetToken.address, 0, gameEndTime);
    await poolGame.connect(player1).sendToPool1(ethers.utils.parseEther("100"));
    await poolGame.connect(player2).sendToPool1(ethers.utils.parseEther("200"));
    await poolGame.connect(player3).sendToPool2(ethers.utils.parseEther("300"));
  
    await ethers.provider.send("evm_increaseTime", [24 * 60 * 60]); // Increase time to 24 hours later

    await expect(poolGame.withdraw(player1Address))
        .to
        .changeTokenBalance(assetToken, player1, ethers.utils.parseEther("99")); // 100 x 99%

    await expect(poolGame.withdraw(player2Address))
        .to
        .changeTokenBalance(assetToken, player2, ethers.utils.parseEther("198")); // 200 x 99%

    await expect(poolGame.withdraw(player3Address))
        .to
        .changeTokenBalance(assetToken, player3, ethers.utils.parseEther("297")); // 300 x 99%

    await expect(poolGame.withdrawFee(await owner.getAddress()))
        .to
        .changeTokenBalance(assetToken, owner, ethers.utils.parseEther("6")); // 600 x 1%
  });
});