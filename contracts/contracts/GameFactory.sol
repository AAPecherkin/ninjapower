// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/ClonesUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/IERC20Upgradeable.sol";
import "./interfaces/INinjaPower.sol";
import "./wCORE.sol";

contract GameFactory is Initializable {
    address public masterContract;
    address payable public assetToken;
    address public currentGameAddress;
    uint256 public reward;
    uint256 public beginTime;
    uint256 private _counter;

    mapping(address => address[]) public games;

    event ContractCreated(address newContract);

    address public constant ADMIN_ADDRESS =
        0x04e622503C1542bFdaE252f076603521FEa31E16;

    function initialize(
        address masterContract_,
        address payable assetToken_,
        uint256 reward_
    ) public initializer {
        masterContract = masterContract_;
        assetToken = assetToken_;
        reward = reward_;
        beginTime = block.timestamp;
    }

    fallback() external payable {}

    receive() external payable {}

    function _addGame(address account, address gameAddress) internal {
        uint256 length = games[account].length;
        if (length > 0 && games[account][length - 1] == gameAddress) return;

        games[account].push(gameAddress);
    }

    function _createContract(
        address assetToken_,
        uint256 rewardAmount_
    ) internal {
        uint256 counter = getCurrentGameIdx();
        if (counter <= _counter) return;
        _counter = counter;

        address newContract = ClonesUpgradeable.cloneDeterministic(
            masterContract,
            bytes32(_counter)
        );

        // Initialize the NinjaPower contract
        IERC20Upgradeable(assetToken_).approve(newContract, rewardAmount_);
        uint256 gameEndTime = beginTime + (_counter * 6 minutes) - 1 minutes; // 5 min
        INinjaPower(newContract).startGame(
            assetToken_,
            address(this),
            gameEndTime
        );

        currentGameAddress = newContract;

        emit ContractCreated(newContract);
    }

    function sendToPool1(uint256 amount) external payable {
        require(msg.value == amount, "Transfer value!");

        WCORE(assetToken).deposit{value : msg.value}();

        _createContract(assetToken, reward);
        WCORE(assetToken).approve(currentGameAddress, amount);
        INinjaPower(currentGameAddress).sendToPool1From(msg.sender, amount);

        _addGame(msg.sender, currentGameAddress);
    }

    function sendToPool2(uint256 amount) external payable {
        require(msg.value == amount, "Transfer value!");

        WCORE(assetToken).deposit{value : msg.value}();

        _createContract(assetToken, reward);
        WCORE(assetToken).approve(currentGameAddress, amount);
        INinjaPower(currentGameAddress).sendToPool2From(msg.sender, amount);

        _addGame(msg.sender, currentGameAddress);
    }

    function withdraw(address payable to) external {
        uint256 length = games[to].length;

        if (length == 0) return;

        uint256 total = 0;

        // all games except the last one
        for (uint256 i = 0; i < length - 1; i++) {
            uint256 wa = INinjaPower(games[to][i]).previewWithdrawal(to);
            if (wa > 0) {
                INinjaPower(games[to][i]).withdrawCore(to, wa);
                total += wa;
            }
        }

        // last game
        if (INinjaPower(games[to][length - 1]).isGameEnded()) {
            uint256 wa = INinjaPower(games[to][length - 1]).previewWithdrawal(to);
            if (wa > 0) {
                INinjaPower(games[to][length - 1]).withdrawCore(to, wa);
                total += wa;
            }
            delete games[to];
        } else {
            delete games[to];
            _addGame(to, currentGameAddress);
        }

        // uint256 balance = WCORE(assetToken).balanceOf(to);
        // total = total > balance ? balance : total;
        WCORE(assetToken).withdraw(total);

        (bool sent,) = to.call{value: total}("");
        require(sent, "GameFactory: Failed to send Ether");
    }

    function previewWithdrawal(address to) external view returns (uint256) {
        uint256 length = games[to].length;

        if (length == 0) return 0;

        uint256 totalAmount = 0;
        // all games except the last one
        for (uint256 i = 0; i < games[to].length - 1; i++) {
            totalAmount += INinjaPower(games[to][i]).previewWithdrawal(to);
        }

        // last game
        if (INinjaPower(games[to][length - 1]).isGameEnded()) {
            totalAmount += INinjaPower(games[to][length - 1]).previewWithdrawal(
                to
            );
        }

        return totalAmount;
    }

    function getAddress(uint256 idx) public view returns (address) {
        return
            ClonesUpgradeable.predictDeterministicAddress(
                masterContract,
                bytes32(idx)
            );
    }

    function getCurrentGameIdx() public view virtual returns (uint256) {
        return (block.timestamp - beginTime) / 6 minutes + 1;
    }

    function getCurrentGameEndTime() public view returns (uint256) {
        uint256 id = getCurrentGameIdx();
        return beginTime + (id * 6 minutes) - 1 minutes; // 5 minutes
    }

    function getCurrentAddress() public view returns (address) {
        uint256 id = getCurrentGameIdx();
        return getAddress(id);
    }
}
