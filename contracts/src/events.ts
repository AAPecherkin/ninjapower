import { Log } from '@ethersproject/abstract-provider';
import { JsonRpcProvider } from "@ethersproject/providers";
import { getContractsForChainOrThrow, getGameByIdx } from "./contracts";
import { BigNumber } from 'ethers';

export interface IUserStats {
    pool1Points: BigNumber,
    pool2Points: BigNumber,
    pool1Tokens: BigNumber,
    pool2Tokens: BigNumber
}

export interface IEventInterval {
    from: number;
    to: number;
    addresses: Record<string, IUserStats>;
}

export async function getIntervalStats(provider: JsonRpcProvider, from: number, to: number) {
    const chainId = (await provider.getNetwork()).chainId;
    const contracts = await getContractsForChainOrThrow(chainId, provider);

    const promises = [];
    for (let gameId = from; gameId <= to; gameId++) {
        const promise = (async () => {
          return await contracts.gameFactoryContract.getAddress(
            gameId,
          );
        })();

        promises.push(promise);
    }
    const resolvedResults = await Promise.allSettled(promises);
    
    const addresses = resolvedResults
        .filter(result => result.status == 'fulfilled')
        .map(result => (result as any).value);


    const game = await getGameByIdx(contracts.gameFactoryContract, 1);
    const filter = game.filters.PoolReplenished();
    const finalFilter = {
        ...filter,
        address: addresses
    }

    const logs: Log[] = await provider.send('eth_getLogs', [finalFilter]);   
    const parsedLogs = logs.map(log => game.interface.parseLog(log));

    const results = parsedLogs.reduce((prev, cur) => {
        const user = cur.args.user;
        const assets = cur.args.assetAmount;
        const points = cur.args.pointsAmount;
        if (prev[user] == undefined) {
            prev[user] = {
                pool1Points: BigNumber.from(0),
                pool2Points: BigNumber.from(0),
                pool1Tokens: BigNumber.from(0),
                pool2Tokens: BigNumber.from(0)
            };
        }

        if (cur.args.poolId) {
            prev[user].pool1Points = prev[user].pool1Points.add(points);
            prev[user].pool1Tokens = prev[user].pool1Tokens.add(assets);
        } else {
            prev[user].pool2Points = prev[user].pool2Points.add(points);
            prev[user].pool2Tokens = prev[user].pool2Tokens.add(assets);
        }

        return prev;
    }, {} as Record<string, IUserStats>);

    return {
        from,
        to,
        addresses: results
    }
}