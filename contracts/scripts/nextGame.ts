import { ethers } from "hardhat";
import { getContractsForChainOrThrow, getGameByIdx } from "../src";
import { GameFactory } from "../typechain-types";


async function main() {
    const [deployer] = await ethers.getSigners();
    const chainId = await deployer.getChainId();
    console.log("deployer ", deployer.address);
    
    const GameFactory = await ethers.getContractFactory("GameFactory");
    const factory = GameFactory.attach("0x2dc0E455d803e3f57c3F87aB256698BAAA1eA04a");

    const gameId = await factory.getCurrentGameIdx();

    // const newMasterContract = "0x1d3615a866296D546E40719Ba8B246b7BDF993da";
    // const tx = await factory.testnet_nextGame();

    // await tx.wait();

    // console.log("Finished!");

    // const gameAddr = factory.getAddress(41);
    // const NinjaPower = await ethers.getContractFactory("NinjaPowerV2"); 
    // const addr = await factory.getAddress(41);
    // const NinjaPower = NinjaPower.attach(addr);
    
    // // const v = await NinjaPower.isGameEnded();
    // // console.log(v);

    // const tx = await NinjaPower.endGame();
    // await tx.wait();
}

main()
    .then(() => {
        console.log("Finished!");
    });