import styles from './rule-statement.module.less';
import React from 'react';
import { Col, Row, Typography } from 'antd';

export interface RuleStatementProps {
  title: string;
  description: React.ReactNode;
}

export function RuleStatement({ description, title }: RuleStatementProps) {
  return (
    <Row className={styles['container']} gutter={48} wrap={false}>
      <Col flex={'1'}>
        <Typography.Title level={4}>{title}</Typography.Title>
      </Col>
      <Col flex={'4'}>
        <Typography.Paragraph>{description}</Typography.Paragraph>
      </Col>
    </Row>
  );
}

export default RuleStatement;
