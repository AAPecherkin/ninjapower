import { makeAutoObservable } from 'mobx';
import { ChainService } from '../services/chain-service';

export class ChainStore {
  currentChainId = -1;

  constructor() {
    makeAutoObservable(this);
  }

  onChainChanged = (chain: string) => {
    this.currentChainId = parseInt(chain, 16);
  };

  fetchCurrentChain = async () => {
    const chainIdHex = await ChainService.getMetamaskCurrentChainId();
    this.onChainChanged(chainIdHex);

    return this.currentChainId;
  };
}
