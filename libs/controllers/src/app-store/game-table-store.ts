import { makeAutoObservable } from 'mobx';
import { GameStore } from './game-store';

export class GameTableStore {
  youSendValue = '';
  teamGetsValue = '';

  constructor(private readonly gameStore: GameStore) {
    makeAutoObservable(this);
  }

  updateYouSendValue = (newValue: string) => {
    this.youSendValue = newValue;
    this.teamGetsValue = Number(
      +newValue * +this.gameStore.currentRateValue,
    ).toFixed(2);
  };

  updateTeamGetsValue = (newValue: string) => {
    this.teamGetsValue = newValue;
    this.youSendValue = Number(
      +newValue / +this.gameStore.currentRateValue,
    ).toFixed(2);
  };
}
