declare let window: any;

import { makeAutoObservable } from 'mobx';
import { Web3Service } from '../services/web3-service';
import { ChainStore } from './chain-store';
import { ChainService } from '../services/chain-service';
import { CHAIN_IDS } from '../types/constants';
import LogRocket from 'logrocket';
import setupLogRocketReact from 'logrocket-react';
import * as Sentry from '@sentry/react';

export class Web3Store {
  currentAddress?: string = undefined;
  isMetamaskInstalled = !!window.ethereum;

  constructor(private readonly chainStore: ChainStore) {
    makeAutoObservable(this);
  }

  init = async () => {
    let accounts = await Web3Service.initMetamask();
    this.onAccountChanged(accounts);
    if (this.isMetamaskInstalled) {
      if (!accounts.length) {
        accounts = await window.ethereum.request({
          method: 'eth_requestAccounts',
        });
      }
      this.onAccountChanged(accounts);
      const [, changed] = await Web3Service.fetchSigner(accounts);
      if (changed) {
        location.reload();
      }
    }
    const chainId = await this.chainStore.fetchCurrentChain();
    await Web3Service.fetchContract(
      chainId || CHAIN_IDS[ChainService.allowedChain],
    );
  };

  onAccountChanged = (accounts: string[]) => {
    if (accounts.length) {
      this.currentAddress = accounts[0];

      LogRocket.init('4vsnbg/tow-oblzj');
      setupLogRocketReact(LogRocket);
      LogRocket.getSessionURL((sessionURL) => {
        Sentry.configureScope((scope) => {
          scope.setExtra('sessionURL', sessionURL);
        });
      });
      LogRocket.identify(this.currentAddress);
    }
  };
}
