import styles from './nav-icon.module.less';
import { Icon, LinkWithQuery } from '@ninja-power/ui-components';
import React from 'react';
import classNames from 'classnames';

export interface NavIconProps {
  to: string;
  icon: React.ReactNode;
  active?: boolean;
  external?: boolean;
}

export function NavIcon({ icon, to, active, external }: NavIconProps) {
  return (
    <LinkWithQuery
      to={to}
      className={classNames(styles['container'], {
        [styles['active']]: active,
      })}
      external={external}
    >
      <Icon icon={icon} size={'large'}></Icon>
    </LinkWithQuery>
  );
}

export default NavIcon;
