import styles from './winners-controller.module.less';
import Winners from '../winners/winners';
import { observer } from 'mobx-react-lite';
import { useStore } from '@ninja-power/controllers';
import { useEffect } from 'react';

/* eslint-disable-next-line */
export interface WinnersControllerProps {}

export const WinnersController = observer((props: WinnersControllerProps) => {
  const { winnersStore } = useStore();

  useEffect(() => {
    winnersStore.update();
  }, []);

  return (
    <div className={styles['container']}>
      <Winners winners={winnersStore.winners} />
    </div>
  );
});

export default WinnersController;
