import { Col, Grid, Layout, Row } from 'antd';
import { InfoIcon, NinjaPowerIcon, TableIcon } from '@ninja-power/ui-icons';
import MetamaskButton from '../metamask-button/metamask-button';
import styles from './app-header-controller.module.less';
import Navbar from '../navbar/navbar';
import { ERoutes } from '../../routes';
import { ChainService, useStore } from '@ninja-power/controllers';
import { observer } from 'mobx-react-lite';
import { CHAIN_IDS } from '@ninja-power/controllers';
import { LinkWithQuery } from '@ninja-power/ui-components';

/* eslint-disable-next-line */
export interface AppHeaderProps {}

export const AppHeaderController = observer((props: AppHeaderProps) => {
  const { lg } = Grid.useBreakpoint();
  const { web3Store, chainStore } = useStore();

  const isConnected =
    !!web3Store.currentAddress &&
    chainStore.currentChainId === CHAIN_IDS[ChainService.allowedChain];

  return (
    <Layout.Header className={styles['header']}>
      <Row
        justify={'space-between'}
        align={'middle'}
        className={styles['container']}
        wrap={false}
        gutter={64}
      >
        <Col flex={'1'}>
          <Row>
            <LinkWithQuery to={ERoutes.MAIN} className={styles['logo']}>
              <NinjaPowerIcon />
            </LinkWithQuery>
          </Row>
        </Col>
        <Col flex={lg ? '320px' : '1'}>
          <Navbar
            items={[
              {
                // to: ERoutes.TABLE_WINNERS,
                to: ERoutes.TABLE_CALCULATE,
                icon: <TableIcon />,
              },
              {
                to: ERoutes.TABLE_RULES,
                icon: <InfoIcon />,
              },
            ]}
          />
        </Col>
        <Col flex={'1'} className={styles['metamask-button']}>
          <MetamaskButton
            isConnected={isConnected}
            connectButtonProps={{
              onClick: web3Store.init,
            }}
            isMetamaskInstalled={web3Store.isMetamaskInstalled}
          />
        </Col>
      </Row>
    </Layout.Header>
  );
});

export default AppHeaderController;
