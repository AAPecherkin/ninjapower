import styles from './rock-team-stats.module.less';
import {
  Button,
  Col,
  ConfigProvider,
  Input,
  Row,
  Typography,
  Grid,
  theme,
  ButtonProps,
  Tooltip,
} from 'antd';
import classNames from 'classnames';
import React, { useMemo } from 'react';
import { digitSplitter } from '@ninja-power/utils';
import { currencyName } from '@ninja-power/config';

const flexLeft = '1.5';
const flexRight = '1';

export interface RockTeamStatsProps {
  isRightSide?: boolean;
  points: number;
  inputProps: React.HTMLProps<HTMLInputElement>;
  yourPoints: number;
  plusPointsForTeam: number;
  sendButtonProps: ButtonProps;
  pointsForTeamVisibility: boolean;
  inputTooltipTitle: string;
}

export function RockTeamStats({
  isRightSide,
  points,
  inputProps,
  yourPoints,
  plusPointsForTeam,
  sendButtonProps,
  pointsForTeamVisibility,
  inputTooltipTitle,
}: RockTeamStatsProps) {
  const { lg } = Grid.useBreakpoint();
  const algorithm = useMemo(() => {
    const result = [];

    if (isRightSide) {
      result.push(theme.darkAlgorithm);
    }
    if (!lg) {
      result.push(theme.compactAlgorithm);
    }

    return result;
  }, [isRightSide, lg]);

  return (
    <ConfigProvider
      theme={{
        algorithm,
        token: {
          colorPrimary: '#71B2FF',
        },
        components: {
          Typography: {
            colorTextHeading: isRightSide ? '#fff' : '#000',
            colorText: isRightSide ? '#fff' : '#000',
            colorTextDescription: isRightSide ? '#ffffffaa' : '#000000aa',
          },
          Input: {
            colorBorder: '#71B2FF',
            colorBgContainer: isRightSide ? '#000' : '#fff',
            colorText: isRightSide ? '#fff' : '#000',
          },
          Button: {
            colorBorder: isRightSide ? '#fff' : '#000',
            colorBgContainerDisabled: '#fafafa99',
          },
          Tooltip: {
            colorBgDefault: isRightSide ? '#fff' : '#000',
            colorTextLightSolid: isRightSide ? '#000' : '#fff',
          },
        },
      }}
    >
      <div
        className={classNames(
          styles['container'],
          !lg && styles['container-small'],
          isRightSide && styles['right'],
        )}
      >
        <Row align={'middle'} gutter={8} justify={'start'}>
          <Col>
            <Typography.Title level={2}>
              {digitSplitter(points.toFixed(2))}
            </Typography.Title>
          </Col>
          <Col>
            <Typography.Text>Points</Typography.Text>
          </Col>
        </Row>
        <div className={styles['form-container']}>
          <Row gutter={16}>
            <Col flex={flexLeft}>
              <Typography.Text
                className={classNames(
                  !pointsForTeamVisibility &&
                    styles['points-for-team-text-invisible'],
                )}
                type={'secondary'}
              >
                +{Number(plusPointsForTeam).toFixed(2)}
                &nbsp;points&nbsp;for&nbsp;team
              </Typography.Text>
            </Col>
            <Col flex={flexRight}></Col>
          </Row>
          <Row gutter={[16, 4]} wrap={!lg}>
            <Tooltip
              title={<div>{inputTooltipTitle}</div>}
              trigger={inputTooltipTitle ? 'hover' : []}
            >
              <Col flex={flexLeft}>
                <Input
                  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                  // @ts-ignore
                  size={'large'}
                  bordered
                  suffix={currencyName}
                  {...inputProps}
                />
              </Col>
            </Tooltip>
            <Col flex={flexRight}>
              <Button
                size={'large'}
                type={'primary'}
                block
                {...sendButtonProps}
              >
                SEND
              </Button>
            </Col>
          </Row>
          <Row gutter={16} wrap={!lg}>
            {!!yourPoints && (
              <>
                <Col flex={flexLeft}>
                  <Typography.Text>Your points:</Typography.Text>
                </Col>
                <Col flex={flexRight}>
                  <Typography.Text>
                    {digitSplitter(yourPoints.toFixed(2))}
                  </Typography.Text>
                </Col>
              </>
            )}
          </Row>
        </div>
      </div>
    </ConfigProvider>
  );
}

export default RockTeamStats;
